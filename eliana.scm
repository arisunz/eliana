(import (prefix eliana-module eli:)
        (prefix eliana-argparse argp:)
        (chicken random))

(define DEFAULT-DATA-SET
  "abcdefghijkmnopqrstuvwxyzABCDEFGHIJKMNOPQRSTUVWXYZ1234567890")

(define DEFAULT-KEY-LENGTH "20")

(define HELP-MESSAGE
  '("Usage: eliana [options...]"
    ""
    "`eliana' is a random key generator useful for force-brute attacks that"
    "accept a dictionary from standard input. Let luck guide your mischief."
    ""
    "-l <key length>  set the length of the generated keys"
    "-d <data set>    set the characters to build the keys out of"
    "-b <token>       specify a prefix to the random sequence"
    "-a <token>       specify a postfix to the random sequence"
    "-s <seed>        set a seed for the pseudo random number generator"
    "-c <number>      generate <number> amount of keys and quit"
    "-w change the whole key every iteration, instead of a character at a time (less efficient)"
    ""
    "example (equivalent to default behavior):"
    "$ eliana -l 20 -d abcdefghijkmnopqrstuvwxyzABCDEFGHIJKMNOPQRSTUVWXYZ1234567890"
    "..."
    "Z8P4VQ4h2pXpa8ZyiAsd"
    "Z8P4VQ4h2pXpu8ZyiAsd"
    "Z8P4cQ4h2pXpu8ZyiAsd"
    "Z8P4cQ4k2pXpu8ZyiAsd"
    "Z8P4cQ4k2kXpu8ZyiAsd"
    "Z8P4cQ4k2k1pu8ZyiAsd"
    "Z8P4cQ4k2k1pE8ZyiAsd"
    "Z8P4cQ4k2k3pE8ZyiAsd"
    "Z8P4cQ4k2k3pE8qyiAsd"
    "Z8P4ck4k2k3pE8qyiAsd"
    "Z8P4ck4k2k3pE8qyaAsd"
    "Z8P4ck4k2k3pE8qy8Asd"
    "..."))

(define (println . things)
  (for-each display things)
  (newline))

(define (print-help-message)
  (for-each println HELP-MESSAGE))

(define (main)
  (when (argp:argument-present? "-h")
    (print-help-message)
    (exit))
  (let* ((data-set (argp:get-argument-value-or-default "-d" DEFAULT-DATA-SET))
         (key-length (string->number (argp:get-argument-value-or-default "-l" DEFAULT-KEY-LENGTH)))
         (change-entire-key? (if (argp:argument-present? "-w") #t #f))
         (key-container (eli:make-key-container key-length))
         (prefix (argp:get-argument-value-or-default "-b" #f))
         (postfix (argp:get-argument-value-or-default "-a" #f))
         (seed (argp:get-argument-value-or-default "-s" (random-bytes)))
         (max-count (argp:get-argument-value-or-default "-c" #f)))
    (eli:eliana-initialize! seed)
    (let loop ((count 1)
               (max-count (if max-count
                              (string->number max-count)
                              max-count)))
      (eli:change-key! key-container data-set change-entire-key?)
      (eli:print-key key-container #:prefix prefix #:postfix postfix)
      (cond (max-count
             (when (< count max-count) (loop (add1 count) max-count)))
            (else (loop count max-count))))))

(main)
