(module eliana-module
  (export eliana-initialize!
          make-key-container
          change-key!
          print-key)

  (import scheme
          (chicken base)
          (chicken random)
          (chicken keyword)
          r7rs)

  (define (get-random-char data-set)
    (string-ref data-set
                (pseudo-random-integer (string-length data-set))))

  (define-syntax for
    (syntax-rules (in)
      ((for elem in alist body ...)
       (for-each (lambda (elem) body ...) alist))))

  (define-syntax range
    (syntax-rules (from to)
      ((range from n to m)
       (let loop ((ret '())
                  (i n))
         (cond ((> i m) (reverse ret))
               (else (loop (cons i ret)
                           (add1 i))))))))

  (define (change-entire-key! key-container data-set)
    (for n in (range from 0 to (sub1 (string-length key-container)))
         (string-set! key-container n (get-random-char data-set))))

  (define (change-key-character-at-a-time! key-container data-set)
    (string-set! key-container
                 (pseudo-random-integer (string-length key-container))
                 (get-random-char data-set)))

  (define (change-key! key-container data-set #!optional change-entire-key?)
    (if change-entire-key?
        (change-entire-key! key-container data-set)
        (change-key-character-at-a-time! key-container data-set)))

  (define (print-key key-container . args)
    (let ((prefix (get-keyword #:prefix args))
          (postfix (get-keyword #:postfix args)))
      (when prefix (display prefix))
      (display key-container)
      (when postfix (display postfix))
      (newline)))

  (define (make-key-container key-length)
    (make-string key-length #\0))

  (define (eliana-initialize! seed)
    (set-pseudo-random-seed! seed)))
