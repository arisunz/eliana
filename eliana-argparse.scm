(module eliana-argparse
  (export get-switch-value
          value-next?
          argument-present?
          get-argument-value-or-default)

  (import (chicken base)
          (chicken process-context)
          r7rs)

  (define ARGS (argv))

  ;;; Terminology:
  ;;; * token: a comand-line argument
  ;;; * switch: token representing an option, consisting of a #\- and
  ;;;   an alphanumeric character, sometimes followed by a value (another argument)
  ;;; * switch-value: token after a switch that isn's itself a switch

  (define (switch?-test)
    (assert (and (switch? "-a")
                 (not (switch? "banana"))
                 (not (switch? "-wololo")))))

  (define (switch? token)
    (and (string? token)
         (= (string-length token) 2)
         (equal? (string-ref token 0) #\-)))

  (define (next-args token)
    (member token ARGS))

  (define (value-next? switch)
    (and (> (length (next-args switch)) 1)
         (not (switch? (cadr (next-args switch))))))

  (define (argument-present? token)
    (if (next-args token)
        #t #f))

  (define (get-switch-value switch)
    (if (and (switch? switch)
             (argument-present? switch)
             (value-next? switch))
        (cadr (member switch ARGS))
        (error "Argument malformed or not present after" switch)))

  (define (get-argument-value-or-default option default)
    (if (argument-present? option)
        (get-switch-value option)
        default)))
