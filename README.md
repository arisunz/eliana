# eliana
Random key generator for brute-force attacks. Let luck guide your mischief.

# Building
Requires Chicken 5 and the r7rs egg. Clone and run `chicken-install`.

# Usage
Specify the key length with the `-l` option. The `-d` option lets you specify a dataset from which to build the keys. For example, to get 20 character long keys containing alphanumeric characters including upper and lower-case letters, you would run:

`eliana -l 20 -d abcdefghijkmnopqrstuvwxyzABCDEFGHIJKMNOPQRSTUVWXYZ1234567890`

Additionally, the `-b <token>` option specifies a prefix to append to the beggining of the generated sequence. Likewise, `-a <token>` adds a postfix to the end of the sequence.

`eliana` will keep generating keys until halted. Specify a number of keys to generate with `-c N` which will make the program quit after producing `N` number of keys.

By default, each iteration changes only one character of the key. The `-w` option tells `eliana` to change the whole key every iteration. Taking CHICKEN's pseudo random number generator algorithm into consideration, though, this option might be completely unnecessary. See [this](https://en.wikipedia.org/wiki/Well_equidistributed_long-period_linear) for more information.

Finally, `-s <seed>` will let you specify a seed for the PRNG. Otherwise, random bytes from an available entropy source are used.

By itself, `eliana` is not very useful. You need to pipe its output to a program that can read, say, a dictionary, from standard input.

# License
© Ariela Wenner - 2020

Code is licenced under the GNU GPL version 3 or newer. See [COPYING](COPYING.md) for details.
